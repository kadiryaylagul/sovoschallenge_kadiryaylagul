package com.challenge1.UI.step_definitions;

import com.challenge1.UI.pages.SearchInputPage;
import com.challenge1.UI.utilities.BrowserUtils;
import com.challenge1.UI.utilities.ConfigurationReader;
import com.challenge1.UI.utilities.Driver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.junit.Assert.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.naming.directory.SearchResult;
import java.util.ArrayList;
import java.util.List;

public class searchFuncSteps {

    SearchInputPage searchInputPage = new SearchInputPage();
    String actualTitle;

    @Given("the user is on the amazon homepage")
    public void the_user_is_on_the_amazon_homepage() {

        Driver.get().get(ConfigurationReader.get("url"));

    }

    @When("user enter valid product name")
    public void user_enter_valid_product_name() {
        searchInputPage.SearchInput.sendKeys("bebek");
        searchInputPage.searchButton.click();
    }

    @Then("result should be same with the given product")
    public void result_should_be_same_with_the_given_product() {
        String resultText = searchInputPage.searchResult.getText();
        Assert.assertTrue(resultText.contains("bebek"));
    }


    @When("user enters nothing and press search button")
    public void user_enters_nothing_and_press_search_button() {
        actualTitle = Driver.get().getTitle();
        searchInputPage.searchButton.click();

    }

    @Then("it should show the same page")
    public void it_should_show_the_same_page() {

        String expectedTitle = "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more";
        Assert.assertEquals(expectedTitle, actualTitle);
    }

    @When("user types any query {string}")
    public void user_types_any_query(String query) {
        searchInputPage.SearchInput.sendKeys(query);
        BrowserUtils.waitFor(2);
    }


    @Then("it should show list of product suggestion starting with particular query {string}")
    public void it_should_show_list_of_product_suggestion_starting_with_particular_query(String query) {

        Assert.assertTrue(searchInputPage.checkSuggestionResult(query));

    }

    @And("number of {int} rows should be shown in dropdown list")
    public void number_of_rows_should_be_shown_in_dropdown_list(Integer int1) {
        Assert.assertTrue(searchInputPage.checkSuggestionNumber());

    }



    @When("user search a product by ASIN number with {int} characters")
    public void user_search_a_product_by_ASIN_number_with_characters(Integer int1) {
        searchInputPage.SearchInput.sendKeys("B00LH3DMUO");
        BrowserUtils.waitFor(2);
        searchInputPage.searchButton.click();
        BrowserUtils.waitFor(2);
    }


    @Then("user should find only that one product")
    public void user_should_find_only_that_one_product() {
        String resultText = searchInputPage.searchResult.getText();
        Assert.assertTrue(resultText.contains("1 result for"));
    }

    @When("user searchs a product by ASIN number with more than {int} characters")
    public void user_searchs_a_product_by_ASIN_number_with_more_than_characters(Integer int1) {
        searchInputPage.SearchInput.sendKeys("B00LH3DMUOR");
        BrowserUtils.waitFor(2);
        searchInputPage.searchButton.click();
        BrowserUtils.waitFor(3);
    }

    @Then("user should see {string} warning")
    public void user_should_see_warning(String str) {
        String resultText = searchInputPage.nullSearchResult.getText();

        Assert.assertTrue(resultText.contains(str));
    }

    @When("user types symbols like {string}")
    public void user_types_symbols_like(String symbols) {
        searchInputPage.SearchInput.sendKeys(symbols);
        BrowserUtils.waitFor(2);
    }

    @Then("user should not see suggestions")
    public void user_should_not_see_suggestions() {
        Assert.assertTrue(searchInputPage.zeroSuggestion());
    }


}
