package com.challenge1.UI.pages;


import com.challenge1.UI.utilities.Driver;
import org.openqa.selenium.support.PageFactory;




public abstract class  BasePage {

    public BasePage() {

        PageFactory.initElements(Driver.get(), this);
    }

}


