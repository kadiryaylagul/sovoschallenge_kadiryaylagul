package com.challenge1.UI.pages;

import com.challenge1.UI.utilities.BrowserUtils;
import com.challenge1.UI.utilities.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchInputPage extends BasePage {

    @FindBy(css = "input[id='nav-search-submit-button']")
    public WebElement searchButton;

    @FindBy(css = "select[class='nav-search-dropdown searchSelect nav-progressive-attrubute nav-progressive-search-dropdown']")
    public WebElement All;

    @FindBy(css = "select[class='nav-search-dropdown searchSelect nav-progressive-attrubute nav-progressive-search-dropdown']")
    public List <WebElement> AllDepartments;

    @FindBy(css = "input[id='twotabsearchtextbox']")
    public WebElement SearchInput;

    @FindBy(css = "div[class='a-section a-spacing-small a-spacing-top-small']")
    public WebElement searchResult;

    @FindBy(css = "div[class='a-section a-spacing-base a-spacing-top-medium']")
    public WebElement nullSearchResult;


    @FindBy(css = "#suggestions>div")
    public List<WebElement> suggestions;

    public boolean checkSuggestionResult(String query) {
        List<String> suggestionText = BrowserUtils.getElementsText(suggestions);
        if (suggestionText.size() == 11) {
            suggestionText.remove(1);
        }
        for (String suggestion : suggestionText) {
            if (!suggestion.substring(0, (query.length())).equalsIgnoreCase(query)) {
                return false;
            }

        }
        return true;
    }

    public boolean checkSuggestionNumber() {
        if (suggestions.size() > 12) {
            return false;
        } else {
            return true;
        }
    }
    public boolean zeroSuggestion() {
        if (suggestions.size() == 0) {
            return true;
        } else {
            return false;
        }
    }


}







