package com.challenge2.API;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.when;
import static org.hamcrest.MatcherAssert.*;
import static org.testng.Assert.*;


import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.*;

import io.restassured.path.json.JsonPath;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.util.Base64;
import java.util.List;

public class getRequestMocki {

    @BeforeClass
    public void beforeclass() {
        RestAssured.baseURI = "https://mocki.io/v1";
    }

    // After GET request, verify that  notifications should return the following countries: BR, AR
    @Test
    public void test1() {

        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();

        List<String> notifications = jsonPath.getList("data.notifications");

        for (int i = 0; i < notifications.size(); i++) {

            String city = jsonPath.getString("data.notifications.metadata[" + i + "].country");
//            System.out.println("city = " + city);
            assertThat(city, Matchers.either(Matchers.is("BR")).or(Matchers.is("AR")));

        }


    }

    // After GET request, verify that perPage value should correspond to the number of notifications retrieved
    @Test
    public void test2() {
        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();
        List<String> notifications = jsonPath.getList("data.notifications");

        int perPageValue = jsonPath.getInt("data.pageState.perPage");
        assertEquals(perPageValue, notifications.size());
    }

    //  Verify that content of notifications should be a xml encoded on Base64
    @Test
    public void test3() {
        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();

        List<String> notifications = jsonPath.getList("data.notifications");


        for (int i = 0; i < notifications.size(); i++) {


            String content = jsonPath.getString("data.notifications.content[" + i + "]");

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedContent = decoder.decode(content);
            String dcdContent = new String(decodedContent, StandardCharsets.UTF_8);

            try {
                SAXParserFactory.newInstance().newSAXParser().getXMLReader()
                        .parse(new InputSource(new StringReader(dcdContent)));

            } catch (ParserConfigurationException | SAXException | IOException ex) {
                System.out.println("Invalid XML");

            }
        }
    }


    // Verify that notificationId should be a valid GUID
    @Test
    public void test4() {
        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();


        List<String> notifications = jsonPath.getList("data.notifications");

        for (int i = 0; i < notifications.size(); i++) {

            String notificationId = jsonPath.getString("data.notifications.notificationId[" + i + "]");
            System.out.println("notificationId = " + notificationId);


            String regex
                    = "^[{]?[0-9a-fA-F]{8}"
                    + "-([0-9a-fA-F]{4}-)"
                    + "{3}[0-9a-fA-F]{12}[}]?$";


            Pattern p = Pattern.compile(regex);


            Matcher m = p.matcher(notificationId);

            assertTrue(m.matches(), "notificationId " + (i + 1) + " is not a valid GUID");


        }

    }

    @DataProvider(name = "provideContentId")
    public Object[][] provideContentId() {
        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();

        List<String> contents = jsonPath.getList("data.notifications.content");

        List<String> notificationIDs = jsonPath.getList("data.notifications.notificationId");

        Object[][] data = new Object[contents.size()][2];

        for (int i = 0; i < contents.size(); i++) {

            data[i][0] = contents.get(i);
            data[i][1] = notificationIDs.get(i);
        }
        return data;
    }


    // Verify that notificationId should correspond to ID inside content xml document
    @Test(dataProvider = "provideContentId")
    public void test5(String content, String notificationID) {

        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedContent = decoder.decode(content);
        String dcdContent = new String(decodedContent, StandardCharsets.UTF_8);

//          System.out.println(dcdContent);
        System.out.println("notificationId = " + notificationID);

        String ID = dcdContent.substring(dcdContent.indexOf("<ID>") + ("<ID>").length(),
                dcdContent.indexOf("</ID>"));
        System.out.println("ID = " + ID);
        assertEquals(ID, notificationID);

    }
// Verify that 200 notifications should have "Document Authorized" on StatusReason and
// "Document authorized successfully" on Text fields inside content xml document
    @Test
    public void test6() {

        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();

        List<String> notifications = jsonPath.getList("data.notifications");

        for (int i = 0; i < notifications.size(); i++) {

            String content = jsonPath.getString("data.notifications.content[" + i + "]");

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedContent = decoder.decode(content);
            String dcdContent = new String(decodedContent, StandardCharsets.UTF_8);

            String statusCode = dcdContent.substring(dcdContent.indexOf("<StatusReasonCode listID=\"StatusCode\">")
                    + ("<StatusReasonCode listID=\"StatusCode\">").length(), dcdContent.indexOf("</StatusReasonCode>"));
//            System.out.println(statusCode);

            String statusReason = dcdContent.substring(dcdContent.indexOf("<StatusReason>") + ("<StatusReason>").length(),
                    dcdContent.indexOf("</StatusReason>"));
//            System.out.println(statusReason);

            String textXML = dcdContent.substring(dcdContent.indexOf("<Text>") + ("<Text>").length(), dcdContent.indexOf("</Text>"));
//            System.out.println(textXML);


            if (statusCode.equals("200")) {
                assertEquals(statusReason, "Document Authorized");
                assertEquals(textXML, "Document authorized successfully");

            }


        }

    }
//	Verify that 400 notifications should have "Document Rejected" on StatusReason and
//	"Document was rejected by tax authority" on Text fields inside content xml document
    @Test
    public void test7() {

        Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

        JsonPath jsonPath = response.jsonPath();

        List<String> notifications = jsonPath.getList("data.notifications");

        for (int i = 0; i < notifications.size(); i++) {

            String content = jsonPath.getString("data.notifications.content[" + i + "]");

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedContent = decoder.decode(content);
            String dcdContent = new String(decodedContent, StandardCharsets.UTF_8);

            String statusCode = dcdContent.substring(dcdContent.indexOf("<StatusReasonCode listID=\"StatusCode\">")
                    + ("<StatusReasonCode listID=\"StatusCode\">").length(), dcdContent.indexOf("</StatusReasonCode>"));
//            System.out.println(statusCode);

            String statusReason = dcdContent.substring(dcdContent.indexOf("<StatusReason>") + ("<StatusReason>").length(),
                    dcdContent.indexOf("</StatusReason>"));
//            System.out.println(statusReason);

            String textXML = dcdContent.substring(dcdContent.indexOf("<Text>") + ("<Text>").length(), dcdContent.indexOf("</Text>"));
//            System.out.println(textXML);

            if (statusCode.equals("400")) {
                assertEquals(statusReason, "Document Rejected");
                assertEquals(textXML, "Document was rejected by tax authority");

            }

        }
    }
//	Automation should display a warn in case of any rejected notification
        @Test
        public void test8() {

            Response response = when().get(baseURI + "/19430625-2b1c-492a-925f-8b4921964ac3");

            JsonPath jsonPath = response.jsonPath();

            List<String> notifications = jsonPath.getList("data.notifications");

            for (int i = 0; i < notifications.size(); i++) {

                String content = jsonPath.getString("data.notifications.content[" + i + "]");

                Base64.Decoder decoder = Base64.getDecoder();
                byte[] decodedContent = decoder.decode(content);
                String dcdContent = new String(decodedContent, StandardCharsets.UTF_8);

                String statusCode = dcdContent.substring(dcdContent.indexOf("<StatusReasonCode listID=\"StatusCode\">")
                        + ("<StatusReasonCode listID=\"StatusCode\">").length(), dcdContent.indexOf("</StatusReasonCode>"));
//            System.out.println(statusCode);

                String statusReason = dcdContent.substring(dcdContent.indexOf("<StatusReason>") + ("<StatusReason>").length(),
                        dcdContent.indexOf("</StatusReason>"));
//            System.out.println(statusReason);

                String textXML = dcdContent.substring(dcdContent.indexOf("<Text>") + ("<Text>").length(), dcdContent.indexOf("</Text>"));
//            System.out.println(textXML);

                if (statusCode.equals("400")) {
                    System.out.println((i + 1) + ". content of notifications was rejected");
                }

            }

    }
}