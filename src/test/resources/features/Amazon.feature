
Feature:Search functionality of the Amazon website


  Background: Homepage
    Given the user is on the amazon homepage

  Scenario: Searching with a product name

    When user enter valid product name
    Then result should be same with the given product


  Scenario: Null search

    When user enters nothing and press search button
    Then it should show the same page

  Scenario Outline: searching a query

    When user types any query "<query>"
    Then it should show list of product suggestion starting with particular query "<query>"
    And number of 10 rows should be shown in dropdown list
    Examples:
      | query  |
      | a      |
      | laptop |
      | car    |
      | phone  |
      | apple  |


  Scenario: Searching with ASIN number
    When user search a product by ASIN number with 10 characters
    Then user should find only that one product

  @wip
  Scenario: Searching with not valid ASIN number
    When user searchs a product by ASIN number with more than 10 characters
    Then user should see "No results for" warning

  Scenario: Searching with symbols
    When user types symbols like "/(&%"
    Then user should not see suggestions











