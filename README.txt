test-automation-SOVOS
Sovos Challenge project for test automation, covering UI acceptance, API acceptance testing:
The UI framework created using Cucumber BDD in Gherkin language.
For assertion in UI, Junit is used.
The API framework is created using TestNG.

Concepts Included
Cucumber BDD testing for UI
Testng testing for API
Shared state across cucumber step definitions
Dependency injection
Page Object pattern
Common web page interaction methods
Common api interaction methods
Commonly used test utility classes


Tools
Maven
Cucumber-JVM
JUnit
TestNG
Selenium Webdriver
RestAssured


Requirements
In order to utilise this project you need to have the following installed locally:
Maven 3.6.3
Chrome and Chromedriver (UI tests use Chrome by default, can be changed in config)
Java 15 - version 15.0.2
IntelliJ IDEA - version 2021.1.1

Dependencies:
In order to utilise this project you need to have following dependencies in pom.xml
and you can find them https://mvnrepository.com

Selenium-version 3.141.59
Webdrivermanager - version 4.3.1
Cucumber-java - version 5.7.0
Cucumber-junit - version 5.7.0
Rest-assured - version 4.3.2
TestNG - version 7.1.0
Junit - version 4.12

Plugins
maven-compiler-plugin - version 3.8.1
maven-surefire-plugin - version 3.0.0-M4
maven-cucumber-reporting - version 5.0.0

Usage
The project is broken into separate modules for API, UI.
Each of these modules can be utilised independently of the others using maven profiles.

To run API modules, navigate to challenge2-API directory and run:
getRequestMocki class

in terminal with command:
mvn test

under project with right clicking and running
getrequestMocki.xml

To run UI  tests only, navigate to Challenge1-UI-runners directory and run:

CukesRunner


Reporting
Reports for each module are written into their respective /target directories after a successful run.

UI acceptance tests result in a HTML report for each feature in
SovosChallenge\target\default-html-reports\index.html
In the case of test failures, a screen-shot of the UI at the point of failure is embedded into the report.

API acceptance tests result in a HTML report for each feature in
SovosChallenge\target\surefire-reports\emailable-report




